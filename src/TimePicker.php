<?php
/**
 * @copyright Copyright &copy; ExooDev, exoodev.com, 2018
 * @package
 * @version 1.0.0
 */

namespace exoo\timepicker;

use Yii;
use yii\helpers\Html;
use yii\widgets\InputWidget;

/**
 * Create a timepicker which can easily be used by selecting a time value from a pre filled dropdown.
 *
 * For example to use the timepicker with a [[\yii\base\Model|model]]:
 *
 * ```php
 * echo TimePicker::widget([
 *     'model' => $model,
 *     'attribute' => 'time',
 * ]);
 * ```
 *
 * The following example will use the name property instead:
 *
 * ```php
 * echo TimePicker::widget([
 *     'name'  => 'time',
 *     'value'  => $value,
 * ]);
 * ```
 *
 * You can also use this widget in an [[\yii\widgets\ActiveForm|ActiveForm]] using the [[\yii\widgets\ActiveField::widget()|widget()]]
 * method, for example like this:
 *
 * ```php
 * <?= $form->field($model, 'date')->widget(TimePicker::classname(), [
 *     'timeformat' => 'short',
 * ]) ?>
 * ```
 *
 * @author ExooDev <info@exoodev.com>
 * @since 1.0
 */
class Timepicker extends InputWidget
{
    /**
     * @var string the input value.
     */
    public $value;
    /**
     * @var string the default format string to be used to format a [[asTime()|time]].
     * This can be "short", "medium", "long", or "full", which represents a preset format of different lengths.
     *
     * It can also be a custom format as specified in the [ICU manual](http://userguide.icu-project.org/formatparse/datetime#TOC-Date-Time-Format-Syntax).
     * Alternatively this can be a string prefixed with `php:` representing a format that can be recognized by the
     * PHP [date()](http://php.net/manual/en/function.date.php)-function.
     *
     * For example:
     *
     * ```php
     * 'HH:mm:ss' // time in ICU format
     * 'php:H:i:s' // the same time in PHP format
     * ```
     */
    public $timeFormat;
    /**
     * @var string the property
     */
    public $type = 'time';

    /**
     * Initializes the widget.
     * If you override this method, make sure you call the parent implementation first.
     */
    public function init()
    {
        parent::init();

        if ($this->hasModel()) {
            $value = Html::getAttributeValue($this->model, $this->attribute);
        } else {
            $value = $this->value;
        }

        $this->options['type'] = $this->type;

        if ($value !== null && $value !== '') {
            try {
                if ($this->timeFormat === null) {
                    $this->timeFormat = Yii::$app->formatter->timeFormat;
                }
                $this->options['value'] = Yii::$app->formatter->asTime($value, $this->timeFormat);
            } catch(InvalidParamException $e) {
                // ignore exception and keep original value if it is not a valid date
            }
        }
    }

    /**
     * Renders the widget.
     */
    public function run()
    {
        if ($this->hasModel()) {
            $input = Html::activeTextInput($this->model, $this->attribute, $this->options);
        } else {
            $input = Html::textInput($this->name, $this->value, $this->options);
        }

        echo $input;
    }
}
